﻿using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class PhysicalSpecifiedGeographicalFeatureValidator : AbstractValidator<PhysicalSpecifiedGeographicalFeature>
	{
		public PhysicalSpecifiedGeographicalFeatureValidator()
		{
			RuleFor(x => x.geometry)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new GeometryValidator());
			RuleFor(x => x.type)
				.SetValidator(new PhysicalSpecifiedGeographicalFeatureTypeValidator());
		}
	}
}
