﻿using System.Collections.Generic;
using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class EcropReportDocumentValidator : AbstractValidator<CropReportDocument>
	{
		private List<string> supportedTypeCodes = new List<string>() {"CCS", "SMN"};
		public EcropReportDocumentValidator()
		{
			RuleFor(x => x.TypeCode)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
				//.Must(typeCode => supportedTypeCodes.Contains(typeCode))
				//.WithState(x1 => MessageCode.TypeCodeNotSupported);
			RuleFor(x => x.PurposeCode)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
			RuleFor(x => x.SenderSpecifiedParty)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new SpecifiedPartyValidator());
			RuleFor(x => x.RecipientSpecifiedParty)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new SpecifiedPartyValidator());
		}
	}
}