﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class AppliedAgriculturalApplicationRateValidator : AbstractValidator<AppliedSpecifiedAgriculturalApplicationRate>
	{
		public AppliedAgriculturalApplicationRateValidator()
		{
			RuleFor(x => x.AppliedReferencedLocation)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(application => application.SetValidator(new ReferencedLocationValidator()));
		}
	}
}
