﻿using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class ECropRequestValidator :AbstractValidator<ECropRequest>
	{
		public ECropRequestValidator()
		{
			RuleFor(x => x.EcropReportMessage)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new EcropReportMessageValidator());
		}
	}
}
