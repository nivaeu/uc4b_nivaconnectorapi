﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class ApplicableCropProductionAgriculturalProcessValidator :AbstractValidator<ApplicableCropProductionAgriculturalProcess>
	{
		public ApplicableCropProductionAgriculturalProcessValidator()
		{
			RuleFor(x => x.AppliedSpecifiedAgriculturalApplication)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(application => application.SetValidator(new AppliedSpecifiedAgriculturalApplicationValidator()));
		}
	}
}
