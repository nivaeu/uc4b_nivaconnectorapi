﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class ReferencedLocationValidator :AbstractValidator<ReferencedLocation>
	{
		public ReferencedLocationValidator()
		{
			RuleFor(x => x.PhysicalSpecifiedGeographicalFeature)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new PhysicalSpecifiedGeographicalFeatureValidator());
		}
	}
}
