﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class SpecifiedCropPlotValidator : AbstractValidator<SpecifiedCropPlot>
	{
		public SpecifiedCropPlotValidator()
		{
			RuleFor(x => x.GrownFieldCrop)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(crop => crop.SetValidator(new GrownFieldCropValidator()));
			RuleFor(x => x.SpecifiedReferencedLocation)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new ReferencedLocationValidator());
		}
	}
}
