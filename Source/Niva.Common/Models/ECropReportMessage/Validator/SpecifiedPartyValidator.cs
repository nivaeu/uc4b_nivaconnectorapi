﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class SpecifiedPartyValidator : AbstractValidator<SpecifiedParty>
	{
		public SpecifiedPartyValidator()
		{
			RuleFor(x => x.ID)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
			RuleFor(x => x.CountryID)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
			RuleFor(x => x.Name)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
		}
	}
}
