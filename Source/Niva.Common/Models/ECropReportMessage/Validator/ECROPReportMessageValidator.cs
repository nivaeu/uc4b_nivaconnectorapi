﻿using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class EcropReportMessageValidator : AbstractValidator<EcropReportMessage>
	{
		public EcropReportMessageValidator()
		{
			RuleFor(x => x.CropReportDocument)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new EcropReportDocumentValidator());
			RuleFor(x => x.AgriculturalProducerParty)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new AgriculturalProducerPartyValidator());
		}
	}
}