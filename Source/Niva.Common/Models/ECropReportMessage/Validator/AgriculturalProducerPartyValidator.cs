﻿using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class AgriculturalProducerPartyValidator : AbstractValidator<AgriculturalProducerParty>
	{
		public AgriculturalProducerPartyValidator()
		{
			RuleFor(x => x.AgriculturalProductionUnit)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new AgriculturalProductionUnitValidator());
		}
	}
}
