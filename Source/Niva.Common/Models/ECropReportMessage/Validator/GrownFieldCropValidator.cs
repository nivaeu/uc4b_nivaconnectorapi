﻿using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class GrownFieldCropValidator : AbstractValidator<GrownFieldCrop>
	{
		public GrownFieldCropValidator()
		{
			RuleFor(x => x.SpecifiedFieldCropMixtureConstituent)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(mixture => mixture.SetValidator(new SpecifiedFieldCropMixtureConstituentValidator()));
			RuleFor(x => x.ApplicableCropProductionAgriculturalProcess)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(mixture => mixture.SetValidator(new ApplicableCropProductionAgriculturalProcessValidator()));
		}
	}
}
