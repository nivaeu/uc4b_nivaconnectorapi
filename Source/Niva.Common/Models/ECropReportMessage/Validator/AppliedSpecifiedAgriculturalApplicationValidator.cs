﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class AppliedSpecifiedAgriculturalApplicationValidator :AbstractValidator<AppliedSpecifiedAgriculturalApplication>
	{
		public AppliedSpecifiedAgriculturalApplicationValidator()
		{
			RuleFor(x => x.AppliedSpecifiedAgriculturalApplicationRate)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(application => application.SetValidator(new AppliedAgriculturalApplicationRateValidator()));
			RuleFor(x => x.SpecifiedProductBatch)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(application => application.SetValidator(new SpecifiedProductBatchValidator()));
		}
	}
}
