﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Niva.Common.Models.ECropReturnMessage;

namespace Niva.Common.Models.ECropReportMessage.Validator
{
	public class AgriculturalProductionUnitValidator :AbstractValidator<AgriculturalProductionUnit>
	{
		public AgriculturalProductionUnitValidator()
		{
			RuleFor(x => x.SpecifiedCropPlot)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(plot => plot.SetValidator(new SpecifiedCropPlotValidator()));
		}
	}
}
