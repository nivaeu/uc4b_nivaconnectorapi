﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace Niva.Common.Models.ECropReportMessage
{
	/// <summary>
	/// Any substance such as seed, fertilizer, water, gas or chemical applied to an agricultural
	/// field, substrate, construction, plant, animal or product.
	/// </summary>
	public partial class AppliedSpecifiedAgriculturalApplication
	{
		/// <summary>
		/// An array of Agricultural Application Rates
		/// </summary>
		[JsonProperty("AppliedSpecifiedAgriculturalApplicationRate", Required = Required.Always)]
		public AppliedSpecifiedAgriculturalApplicationRate[] AppliedSpecifiedAgriculturalApplicationRate { get; set; }

		/// <summary>
		/// The identifier for this specified agricultural application.
		/// </summary>
		[JsonProperty("ID", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		// ReSharper disable once InconsistentNaming
		public string ID { get; set; }

		/// <summary>
		/// An array of Product Batch
		/// </summary>
		[JsonProperty("SpecifiedProductBatch", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public SpecifiedProductBatch[] SpecifiedProductBatch { get; set; }

		/// <summary>
		/// The total area of an as applied process
		/// </summary>
		[JsonProperty("TotalArea", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public double? TotalArea { get; set; }

		/// <summary>
		/// The total amount of produce or produces applied during and as applied process
		/// </summary>
		[JsonProperty("TotalProductAmount", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public double? TotalProductAmount { get; set; }
	}
}