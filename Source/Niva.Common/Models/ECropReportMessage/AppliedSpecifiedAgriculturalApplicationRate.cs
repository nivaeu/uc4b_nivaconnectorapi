﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace Niva.Common.Models.ECropReportMessage
{
	/// <summary>
	/// An agricultural application rate, as applied on the crop plot.
	/// </summary>
	public partial class AppliedSpecifiedAgriculturalApplicationRate
	{
		/// <summary>
		/// The quantity applied uniformly in this Agricultural Application Rate.
		/// </summary>
		[JsonProperty("AppliedQuantity", Required = Required.Always)]
		public double AppliedQuantity { get; set; }

		/// <summary>
		/// The unit of the quantity applied uniformly in this Agricultural Application Rate. Units
		/// are g, kg, t, tab, 10000 grains and others.
		/// </summary>
		[JsonProperty("AppliedQuantityUnit", Required = Required.Always)]
		public string AppliedQuantityUnit { get; set; }

		/// <summary>
		/// An array of locations, or areas, treated with this specific Applied Agricultural
		/// Application Rate.
		/// </summary>
		[JsonProperty("AppliedReferencedLocation", Required = Required.Always)]
		public ReferencedLocation[] AppliedReferencedLocation { get; set; }
	}
}