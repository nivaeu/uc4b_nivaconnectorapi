﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace Niva.Common.Models.ECropReportMessage
{ 
	public static class Serialize
	{
		public static string ToJson(this ECropRequest self) => JsonConvert.SerializeObject(self, Converter.Settings);
	}
}