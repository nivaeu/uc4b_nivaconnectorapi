﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace Niva.Common.Models.ECropReportMessage
{

	/// <summary>
	/// A practice of cultivating land, raising crops, or treatment of the agricultural produce.
	/// </summary>
	public partial class ApplicableCropProductionAgriculturalProcess
	{
		/// <summary>
		/// The date, time, date time or other date time value for the actual end of the crop
		/// production in this agricultural process.
		/// </summary>
		[JsonProperty("ActualEndDateTime", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string ActualEndDateTime { get; set; }

		/// <summary>
		/// The date, time, date time or other date time value for the actual start of the crop
		/// production in this agricultural process.
		/// </summary>
		[JsonProperty("ActualStartDateTime", Required = Required.Always)]
		public string ActualStartDateTime { get; set; }

		/// <summary>
		/// An array of specified agricultural applications
		/// </summary>
		[JsonProperty("AppliedSpecifiedAgriculturalApplication", Required = Required.Always)]
		public AppliedSpecifiedAgriculturalApplication[] AppliedSpecifiedAgriculturalApplication { get; set; }

		/// <summary>
		/// The code specifying the subordinate type of the agricultural process for this crop
		/// production.
		/// </summary>
		[JsonProperty("SubordinateTypeCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string SubordinateTypeCode { get; set; }

		/// <summary>
		/// The code specifying the type of agricultural process for this crop production.
		/// </summary>
		[JsonProperty("TypeCode", Required = Required.Always)]
		public string TypeCode { get; set; }
	}
}