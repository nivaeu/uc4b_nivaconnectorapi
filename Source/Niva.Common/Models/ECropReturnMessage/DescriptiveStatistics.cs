﻿namespace Niva.Common.Models.ECropReturnMessage
{
    public class DescriptiveStatistics
    {
        /// <summary>
        /// Calculate the population mean
        /// </summary>
        public StatisticValue Mean { get; set; }
        /// <summary>
        /// Calculate the coefficient of variation 
        /// </summary>
        public StatisticValue CoefficientVariation { get; set; }
        /// <summary>
        /// Minimum value
        /// </summary>
        public StatisticValue Minimum { get; set; }
        /// <summary>
        /// Maximum value
        /// </summary>
        public StatisticValue Maximum { get; set; }
    }
}