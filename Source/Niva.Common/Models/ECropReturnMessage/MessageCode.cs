﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace Niva.Common.Models.ECropReturnMessage
{
	/// <summary>
	/// None = 0
	/// MandatoryField = 101
  ///	NullField=102
	/// </summary>
	public enum MessageCode
	{
		None = 0,
		MandatoryField = 101,
		NullField=102,
		TypeCodeNotSupported
	}
}
