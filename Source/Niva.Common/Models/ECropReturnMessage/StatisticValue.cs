﻿namespace Niva.Common.Models.ECropReturnMessage
{
    public class StatisticValue
    {
        /// <summary>
        /// Value
        /// </summary>
        public double Value { get; set; }

        /// <summary>
        /// Unit
        /// </summary>
        public string Unit { get; set; }
    }
}