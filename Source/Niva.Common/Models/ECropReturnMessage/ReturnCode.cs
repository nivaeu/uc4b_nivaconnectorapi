﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

// ReSharper disable InconsistentNaming

namespace Niva.Common.Models.ECropReturnMessage
{
	public enum ReturnCode
	{
		No_errors = 0,
		Info_Messages_present=2,
		Warnings = 5,
		Errors_encountered = 9
	}
}

