﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Collections.Generic;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Niva.Common.Models.ECropReturnMessage
{
	public class EcropReceipt
	{
		/// <summary>
		/// Sender
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("Sender")]
		public string Sender { get; set; }
		
		/// <summary>
		/// Recipient
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("Recipient")]
		public string Recipient { get; set; }
		
		/// <summary>
		/// Purpose
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("Purpose")]
		public string Purpose { get; set; }

		/// <summary>
		/// List of Crop Plot (Field)
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("CropPlot")]
		public List<ReceiptCropPlot> CropPlot { get; set; }

	}
}
