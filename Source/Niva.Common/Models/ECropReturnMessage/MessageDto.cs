﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace Niva.Common.Models.ECropReturnMessage
{
	public class MessageDto
	{
		/// <summary>
		/// Refers to the element where the error occurred
		/// </summary>
		[JsonProperty("Element")]
		public string Element { get; set; }

		/// <summary>
		/// Can be either 'Debug', 'Info', 'Warning', 'Error' - for now only 'Error' will be used
		/// </summary>
		[JsonProperty("Severity")]
		public string Severity { get; set; }

		/// <summary>
		/// Code uniquely identifying the error. Please log errorcodes in document on Sharepoint
		/// </summary>
		[JsonProperty("MessageCode")]
		public int MessageCode { get; set; }

		/// <summary>
		/// Human readable error message in English - FMIS's can return a localized version based on the ErrorCode
		/// </summary>
		[JsonProperty("Message")]
		public string Message { get; set; }

	}
}