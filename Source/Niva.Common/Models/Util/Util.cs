﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/


using Niva.Common.Models.Util.Enums;

namespace Niva.Common.Models.Util
{
	public static class NivaUtil
	{
		public static OrganisationCode GetOrganisationCode(string countryCode)
		{
			return countryCode.ToLower() switch
			{
				"dk" => OrganisationCode.DK,
				"nl" => OrganisationCode.NL,
				_ => OrganisationCode.Unknown
			};
		}

		public static ReportTypeCode GetReportTypeCode(string reportTypeCode)
		{
			return reportTypeCode.ToLower() switch
			{
				"scc" => ReportTypeCode.SCC,
				"smn" => ReportTypeCode.SMN,
				_ => ReportTypeCode.Unknown
			};
		}
	}
}
