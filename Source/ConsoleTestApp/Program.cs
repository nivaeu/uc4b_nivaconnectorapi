﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using Newtonsoft.Json;
using Niva.Common.Models.ECropReportMessage;

namespace ConsoleTestApp
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("NIVA eCrop class tree tester.");
			var eCropMessage = new EcropReportMessage
			{
				CropReportDocument = new CropReportDocument()
					{ControlRequirementIndicator = false,
					RecipientSpecifiedParty = new SpecifiedParty
						{CountryID = "DK", 
						Name = "Receiving PA"
						},
					SenderSpecifiedParty = new SpecifiedParty
						{
							CountryID = "DK",
							Name = "Sending farmer"
						},
					CopyIndicator = false,
					ID = "123"
					},
				AgriculturalProducerParty = new AgriculturalProducerParty()
					{Name = "Sending farmer name",
					ClassificationCode = "Some classification code",
					Description =  "Some descriptive text",
					AgriculturalProductionUnit = new AgriculturalProductionUnit()
					{
						Name = "Next level class"
					},
					ID = "123"
				}
			};
			var options = new JsonSerializerSettings(){NullValueHandling = NullValueHandling.Ignore};
			Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(eCropMessage, Formatting.Indented, options));
			//Console.WriteLine("Hit <Enter>");
			//Console.ReadLine();
		}
	}
}
