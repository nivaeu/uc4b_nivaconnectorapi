/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using Niva.Common.Models.ECropReportMessage;
using Niva.Common.Models.ECropReturnMessage;
using NivaConnectorApi.Infrastructure;
using NivaConnectorApi.Models;
using NivaConnectorApi.Repository.NIVA;
using NivaConnectorApi.Repository.Proprietary.PayingAgencyNL;
using NivaConnectorApi.Repository.Proprietary.SEGES;
using Serilog;

namespace NivaConnectorApi.Controllers.Proprietary.SEGES
{
	/// <summary>
	/// Controller handling retrieval of information from SEGES Datahub/API
	/// </summary>
	[ProducesErrorResponseType(typeof(RequestError))]
	public class SegesDatahubController : BaseProprietaryController
	{
		private readonly PlantApi _plantApiManager;
		private readonly ValidatorApi _validatorApiManager;
		private readonly PayingAgencyApi _payingAgencyApi;

		public SegesDatahubController(PlantApi plantApiManager, ValidatorApi validator, PayingAgencyApi payingAgencyApi)
		{
			_plantApiManager = plantApiManager;
			_validatorApiManager = validator;
			_payingAgencyApi = payingAgencyApi;
		}

        /// <summary>
		/// Gets tasks for the specified farm and harvest year from PlantAPI
		/// </summary>
		/// <param name="farmId"></param>
		/// <param name="harvestYear"></param>
		/// <param name="language">Language code. English (en) is default. ISO 639-1: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes </param>
		/// <returns></returns>
		[Route("seges/datahub/farm/{farmId:int}/harvestyear/{harvestYear:int}/tasks")]
		[HttpGet]
		[ProducesResponseType(typeof(List<TaskSummary>), 200)]
		public async Task<IActionResult> GetTasks(int farmId, int harvestYear, [FromQuery] string language = "en")
		{
			var url = $"niva/farms/{farmId}/harvestyear/{harvestYear}/tasks?language={language}";
			(RequestError requestError, List<TaskSummary> tasks) = await _plantApiManager.SendRequestAsync<List<TaskSummary>>(
				Request.HttpContext, "GET", url);
			if (requestError != null)
			{
				return BadRequest(requestError);
			}

			return Ok(tasks);
		}

		/// <summary>
		/// Gets AsApplied VraPrescriptionMaps from PlantAPI
		/// </summary>
		/// <param name="farmId"></param>
		/// <param name="harvestYear"></param>
		/// <param name="id">id if AsApplied VraPrescriptionMaps to retrieve, taken from VraTasks</param>
		/// <param name="language">Language code. English (en) is default. ISO 639-1: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes </param>
		/// <returns></returns>
		[Route("seges/datahub/prescriptionmaps/applied/{id:int}")]
		[HttpGet]
		[ProducesResponseType(typeof(EcropMessage), 200)]
		public async Task<IActionResult> GetEcropMessage(
			int id,
			[FromQuery, BindRequired] int farmId,
			[FromQuery, BindRequired] int harvestYear,
			[FromQuery] string language = "en")
		{
			if (id <= 0)
			{
				return BadRequest();
			}

			var url = $"niva/farms/{farmId}/{harvestYear}/applied/{id}/ecrop";
			(RequestError requestError, EcropMessage reportMessage) = await _plantApiManager.SendRequestAsync<EcropMessage>(Request.HttpContext, "GET", url);
			if (requestError != null)
			{
				return BadRequest();
			}

			return Ok(reportMessage);
		}

		/// <summary>
		/// Gets validation result for the specified Applied VraPrescriptionMaps, from NivaValidator
		/// </summary>
		/// <param name="ids">ids of Applied VraPrescriptionMaps to create ECrop validation message for</param>
		/// <param name="farmId">farm id for field</param>
		/// <param name="harvestYear">harvestYear of field</param>
		/// <param name="language">Language code. English (en) is default. ISO 639-1: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes </param>
		/// <returns></returns>
		[Route("seges/datahub/prescriptionmaps/applied/{ids}/ecropvalidation")]
		[HttpGet]
		[ProducesResponseType(200)]
		[ProducesResponseType(200, Type = typeof(List<ValidationError>))]
		public async Task<IActionResult> ValidateAppliedVraPrescriptionMaps(
			IntegerListParameter ids, [FromQuery, BindRequired] int farmId, [FromQuery, BindRequired] int harvestYear, [FromQuery] string language = "en")
		{
			var id = ids.FirstOrDefault();
			if (id == default)
			{
				return BadRequest();
			}

			return await ValidateAppliedVraPrescriptionMap(id, farmId, harvestYear, language);
		}

		/// <summary>
		/// Gets validation result for the specified Applied VraPrescriptionMap, from NivaValidator
		/// </summary>
		/// <param name="id">id of Applied VraPrescriptionMap to create ECrop validation message for</param>
		/// <param name="farmId">farm id for field</param>
		/// <param name="harvestYear">harvestYear of field</param>
		/// <param name="language">Language code. English (en) is default. ISO 639-1: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes </param>
		/// <returns></returns>
		[Route("seges/datahub/prescriptionmap/applied/{id:int}/ecropvalidation")]
		[HttpGet]
		[ProducesResponseType(200)]
		[ProducesResponseType(200, Type = typeof(List<ValidationError>))]
		public async Task<IActionResult> ValidateAppliedVraPrescriptionMap(
			int id, [FromQuery, BindRequired] int farmId, [FromQuery, BindRequired] int harvestYear, [FromQuery] string language = "en")
		{
			var url = $"niva/farms/{farmId}/{harvestYear}/applied/{id}/ecrop";
			(RequestError requestError, EcropMessage reportMessage) = await _plantApiManager.SendRequestAsync<EcropMessage>(Request.HttpContext, "GET", url);
			if (requestError != null)
			{
				return BadRequest();
			}

			//Validate against NivaValidator
			string reportJson = JsonConvert.SerializeObject(reportMessage);
			(RequestError requestErrorValidator, ECropResponse validationResult) =
				await _validatorApiManager.SendRequestAsync<ECropResponse>(Request.HttpContext,
				"POST", $"ECrop", reportJson);

			if (requestErrorValidator != null)
			{
				return BadRequest(requestErrorValidator);
			}

			var resultingValidationErrors = new List<ValidationError>();

			var noErrors = validationResult.ECROPReturnMessage.ReturnCode == 0;
			if (noErrors)
			{
				return Ok(resultingValidationErrors);
			}

            foreach (MessageDto validationMessage in validationResult.ECROPReturnMessage.Messages)
			{
				object reportMessageEntity = null;
				string reportMessageEntityName = "";

				foreach (string t in validationMessage.Element.Replace("][", "],[").Split(',')) //has form {"<propertyname>[<index>]", "<propertyname2>[<index2>]" }
				{
					object currentReportMessageEntity = reportMessageEntity ?? reportMessage;
					int? propertyIndex = null;
					string[] property = t
						.Replace("['", "")
						.Replace("']", "")
						.Split("["); //has form {"<propertyname>", "<index>]"}

					if (property.Last().Contains("]"))
					{
						propertyIndex = int.Parse(property.Last().Replace("]", ""));
					}

					Type type = currentReportMessageEntity.GetType();
					if (type.GetProperty(property.First(), BindingFlags.Instance |
																								 BindingFlags.Public |
																								 BindingFlags.IgnoreCase) != null)
					{
						PropertyInfo p = type.GetProperty(property.First(), BindingFlags.Instance |
																																 BindingFlags.Public |
																																 BindingFlags.IgnoreCase);

						if (p == null)
						{
							var error = new RequestError
							{
								ErrorCode = (int)ApiErrorCode.UnknownExternalApiError,
								ErrorMessage = $"Validation element contained invalid value. Unable to find relevant ECrop property: {t} not found"
							};

							return NotFound(error);
						}

						reportMessageEntityName = type.ToString().Split(".").Last() + "." + p.Name;

						if (propertyIndex != null && p.PropertyType.IsArray)
						{
							var val = (Array)p.GetValue(currentReportMessageEntity);
							reportMessageEntity = val.GetValue(propertyIndex.Value);
						}
						else
						{
							reportMessageEntity = p.GetValue(currentReportMessageEntity, null);
						}
					}
				}

				resultingValidationErrors.Add(new ValidationError
				{
					ErrorMessage = validationMessage.Message,
					ErrorCode = validationMessage.MessageCode,
					SpecifiedEntityName = reportMessageEntityName,
					SpecifiedEntity = IsSimple(reportMessageEntity?.GetType()) ? reportMessageEntity : null
				});
			}

			Log.Warning("Not valid eCropMessage. Validation messages {@validationMessage}", resultingValidationErrors);

			return Ok(resultingValidationErrors);
		}

		/// <summary>
		/// Gets validation result for the specified Applied VraPrescriptionMaps, from NivaValidator
		/// </summary>
		/// <param name="ids">ids of Applied VraPrescriptionMaps to create ECrop validation message for</param>
		/// <param name="farmId">farm id for field</param>
		/// <param name="harvestYear">harvestYear of field</param>
		/// <param name="language">Language code. English (en) is default. ISO 639-1: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes </param>
		/// <returns></returns>
		[Route("seges/datahub/prescriptionmaps/applied/{ids}/ecropreceipt")]
		[HttpGet]
		[ProducesResponseType(200)]
		[ProducesResponseType(200, Type = typeof(EcropReceipt))]
		public async Task<IActionResult> GetReceiptAppliedVraPrescriptionMaps(
			IntegerListParameter ids, [FromQuery, BindRequired] int farmId, [FromQuery, BindRequired] int harvestYear, [FromQuery] string language = "en")
		{
			var id = ids.FirstOrDefault();
			if (id == default)
			{
				return BadRequest();
			}

			return await GetReceiptAppliedVraPrescriptionMap(id, farmId, harvestYear, language);
		}

		/// <summary>
		/// Gets validation result for the specified Applied VraPrescriptionMap, from NivaValidator
		/// </summary>
		/// <param name="id">id of Applied VraPrescriptionMap to create ECrop validation message for</param>
		/// <param name="farmId">farm id for field</param>
		/// <param name="harvestYear">harvestYear of field</param>
		/// <param name="language">Language code. English (en) is default. ISO 639-1: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes </param>
		/// <returns></returns>
		[Route("seges/datahub/prescriptionmap/applied/{id:int}/ecropreceipt")]
		[HttpGet]
		[ProducesResponseType(200)]
		[ProducesResponseType(200, Type = typeof(EcropReceipt))]
		public async Task<IActionResult> GetReceiptAppliedVraPrescriptionMap(
			int id, [FromQuery, BindRequired] int farmId, [FromQuery, BindRequired] int harvestYear, [FromQuery] string language = "en")
		{
			var url = $"niva/farms/{farmId}/{harvestYear}/applied/{id}/ecrop";
			(RequestError requestError, EcropMessage reportMessage) = await _plantApiManager.SendRequestAsync<EcropMessage>(Request.HttpContext, "GET", url);
			if (requestError != null)
			{
				return BadRequest(); //todo evt better error handling ?
			}

			//Get ECropReceipt from NivaValidator
			string reportJson = JsonConvert.SerializeObject(reportMessage);

			(RequestError requestErrorValidatorReceipt, EcropReceipt receiptResult) = await _validatorApiManager.SendRequestAsync<EcropReceipt>(Request.HttpContext,
				"POST", $"Receipt", reportJson);

			if (requestErrorValidatorReceipt != null)
			{
				requestErrorValidatorReceipt.ErrorCode = (int)ApiErrorCode.ValidatorReceiptError;

				return BadRequest(requestErrorValidatorReceipt);
			}

			if (receiptResult == null)
			{
				var validationError = new RequestError
				{
					ErrorCode = (int)ApiErrorCode.ValidatorError,
					ErrorMessage = "Validator contained errors for ECrop Report"
				};

				return BadRequest(validationError);
			}

			return Ok(receiptResult);
		}

		/// <summary>
		/// Sends ECrop message to Paying Agency in NL
		/// </summary>
		/// <param name="ids">ids of Applied VraPrescriptionMaps to create ECrop message for</param>
		/// <param name="farmId">farm id for field</param>
		/// <param name="harvestYear">harvestYear of field</param>
		/// <param name="language">Language code. English (en) is default. ISO 639-1: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes </param>
		/// <returns></returns>
		/// /// <remarks>Connection to payingagency from localhost requires certificate in the trust store (Certificates - Local Computer > Trusted Root Certification centers > Certificates)</remarks>
        /// <remarks>This is a private root certificate, named; 'Staat der Nederlanden Private Root CA - G1'. This can be downloaded from: https://www.pkioverheid.nl</remarks>
        /// <remarks>On Azure the certificate is uploadet as Public Key Certifikates under TLS/SSL settings at the app service</remarks>
		[Route("seges/datahub/prescriptionmaps/applied/{ids}/payingagency/nl")]
		[HttpGet]
		[ProducesResponseType(200)]
		[ProducesResponseType(200, Type = typeof(ECropResponse))]
		public async Task<IActionResult> SendECropToPayingAgency(IntegerListParameter ids, [FromQuery, BindRequired] int farmId, [FromQuery, BindRequired] int harvestYear, [FromQuery] string language = "en")
		{
            var id = ids.FirstOrDefault();
			if (id == default)
			{
				return BadRequest();
			}

			var url = $"niva/farms/{farmId}/{harvestYear}/applied/{id}/ecrop";
			(RequestError requestError, EcropMessage reportMessage) = await _plantApiManager.SendRequestAsync<EcropMessage>(Request.HttpContext, "GET", url);
			if (requestError != null)
			{
				return BadRequest(); //todo evt better error handling ?
			}

			string reportJson = JsonConvert.SerializeObject(reportMessage);

			Serilog.Log.Logger
				.ForContext("body", reportJson)
				.ForContext("message", "reportjson")
				.Information("reportjson");

			(RequestError requestErrorPayingAgency, ECropResponse payingAgencyResult) = await _payingAgencyApi.SendRequestAsync<ECropResponse>(Request.HttpContext,
				"POST", $"nivaecropmessage", reportJson);

			if (requestErrorPayingAgency != null)
			{
				requestErrorPayingAgency.ErrorCode = (int)ApiErrorCode.PayingAgencyError;

				return BadRequest(requestErrorPayingAgency);
			}

			if (payingAgencyResult == null)
			{
				var validationError = new RequestError
				{
					ErrorCode = (int)ApiErrorCode.UnknownExternalApiError,
					ErrorMessage = "Paying Agency responded with invalid response"
				};

				return BadRequest(validationError);
			}

			if (payingAgencyResult.ECROPReturnMessage.ReturnCode != 0) //0 = no errors
			{
				var error = new RequestError
				{
					ErrorCode = (int)ApiErrorCode.PayingAgencyContainsValidationErrors,
					ErrorMessage = "Paying Agency found validation errors in Ecrop Message"
				};

				return BadRequest(error);
			}

			return Ok();
		}

		/// <summary>
		/// Sends ECrop message to Paying Agency in NL
		/// </summary>
		/// <param name="id">id of Applied VraPrescriptionMap to create ECrop message for</param>
		/// <param name="farmId">farm id for field</param>
		/// <param name="harvestYear">harvestYear of field</param>
		/// <param name="language">Language code. English (en) is default. ISO 639-1: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes </param>
		/// <returns></returns>
		/// <remarks>Connection to payingagency from localhost requires certificate in the trust store (Certificates - Local Computer > Trusted Root Certification centers > Certificates)</remarks>
		/// <remarks>This is a private root certificate, named; 'Staat der Nederlanden Private Root CA - G1'. This can be downloaded from: https://www.pkioverheid.nl</remarks>
		/// <remarks>On Azure the certificate is uploadet as Public Key Certifikates under TLS/SSL settings at the app service</remarks>
		[Route("seges/datahub/prescriptionmap/applied/{id:int}/payingagency/nl")]
		[HttpGet]
		[ProducesResponseType(200)]
		[ProducesResponseType(200, Type = typeof(ECropResponse))]
		public async Task<IActionResult> SendECropToPayingAgency(
			int id, [FromQuery, BindRequired] int farmId, [FromQuery, BindRequired] int harvestYear, [FromQuery] string language = "en")
		{
			var url = $"niva/farms/{farmId}/{harvestYear}/applied/{id}/ecrop";
			(RequestError requestError, EcropMessage reportMessage) = await _plantApiManager.SendRequestAsync<EcropMessage>(Request.HttpContext, "GET", url);
			if (requestError != null)
			{
				return BadRequest(); //todo evt better error handling ?
			}

			var reportJson = JsonConvert.SerializeObject(reportMessage);

			Serilog.Log.Logger
				.ForContext("body", reportJson)
				.ForContext("message", "reportjson")
				.Information("reportjson");

			(RequestError requestErrorPayingAgency, ECropResponse payingAgencyResult) = await _payingAgencyApi.SendRequestAsync<ECropResponse>(Request.HttpContext,
				"POST", $"deliver", reportJson);

			if (requestErrorPayingAgency != null)
			{
				requestErrorPayingAgency.ErrorCode = (int)ApiErrorCode.PayingAgencyError;

				return BadRequest(requestErrorPayingAgency);
			}

			if (payingAgencyResult == null)
			{
				var validationError = new RequestError
				{
					ErrorCode = (int)ApiErrorCode.UnknownExternalApiError,
					ErrorMessage = "Paying Agency responded with invalid response"
				};

				return BadRequest(validationError);
			}

			if (payingAgencyResult.ECROPReturnMessage.ReturnCode != 0) //0 = no errors
			{
				var error = new RequestError
				{
					ErrorCode = (int)ApiErrorCode.PayingAgencyContainsValidationErrors,
					ErrorMessage = "Paying Agency found validation errors in Ecrop Message"
				};

				return BadRequest(error);
			}

			return Ok();
		}

		bool IsSimple(Type type)
		{
			return type.IsPrimitive || type == typeof(string);
		}
	}
}