/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using NivaConnectorApi.Repository.NIVA;
using NivaConnectorApi.Repository.Proprietary.PayingAgencyNL;
using NivaConnectorApi.Repository.Proprietary.SEGES;
using Serilog;

namespace NivaConnectorApi
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public static IConfiguration Configuration { get; set; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddTransient<PlantApi>();
			services.AddTransient<ValidatorApi>();
			services.AddTransient<PayingAgencyApi>();

            //Register the Swagger generator, defining 1 or more Swagger documents
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo
				{
					Title = "NivaConnectorAPI",
					Version = "v1",
					Description = "Api for delivering machine data"
				});

				c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
				{
					Flows = new OpenApiOAuthFlows
					{
						AuthorizationCode = new OpenApiOAuthFlow
						{
							AuthorizationUrl = new Uri(Configuration["Authzserver"] + Configuration["PlantApiOApplication"] + "/oauth/authorize"),
							TokenUrl = new Uri(Configuration["Authzserver"] + Configuration["PlantApiOApplication"] + "/oauth/token"),
							Scopes = new Dictionary<string, string>
							{
								{Configuration["PlantApiScope"], "Plant API"}
							}
						}
					},
					Type = SecuritySchemeType.OAuth2
				});

				var securityScheme = new OpenApiSecurityScheme()
				{
					Reference = new OpenApiReference()
					{
						Id = "oauth2",
						Type = ReferenceType.SecurityScheme
					}
				};

				var securityRequirements = new OpenApiSecurityRequirement()
				{
					{securityScheme, new string[] { }},
				};

				c.AddSecurityRequirement(securityRequirements);

				c.IncludeXmlComments($"{AppDomain.CurrentDomain.BaseDirectory}\\NivaConnectorApi.xml");
			});

			services.AddControllers().AddNewtonsoftJson(
				options => options.SerializerSettings.ContractResolver = new DefaultContractResolver()
				);

			services.AddCors(options =>
			{
				options.AddPolicy("AllowOriginsAnywhere",
					builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
			});

			services.AddMvc()
				.AddJsonOptions(options =>
					{
						options.JsonSerializerOptions.PropertyNamingPolicy = null;
						options.JsonSerializerOptions.IgnoreNullValues = true;
						options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
					}
				);

			services.AddMemoryCache();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (!env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();

			// Anvend Serilog RequestLogging
			app.UseSerilogRequestLogging();

			app.UseRouting();

			app.UseCors("AllowOriginsAnywhere");

			app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

			// Enable middleware to serve generated Swagger as a JSON endpoint.
			app.UseSwagger();

			// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
			// specifying the Swagger JSON endpoint.
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint($"{Configuration["ApplicationSettings:NivaConnectorApiBaseUrl"]}/swagger/v1/swagger.json", "Internal NivaConnectorAPI Documentation");
				c.OAuthClientId(Configuration["OAuthClientId"]);
				c.OAuthAppName("NivaConnectorAPI - Swagger");
				c.OAuth2RedirectUrl(Configuration["OAuth2RedirectUrl"]);
			});
		}
	}
}
