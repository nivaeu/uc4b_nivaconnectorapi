﻿using System;

namespace NivaConnectorApi.Models
{
	public class TaskSummary
	{
		public int Id { get; set; }
		public string Product { get; set; }
		public string Crop { get; set; }
		public DateTime Date { get; set; }
	}
}