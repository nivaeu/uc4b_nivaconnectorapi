/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using NivaConnectorApi.Logging;
using Serilog;
using Serilog.Sinks.Elasticsearch;

namespace NivaConnectorApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile(
                        "appsettings.local.json",
                        optional: true,
                        reloadOnChange: true);
                })
                .UseSerilog(ConfigureLogger);

        private static void ConfigureLogger(HostBuilderContext hostingContext, LoggerConfiguration loggerConfiguration)
        {
            IConfigurationSection configurationSection = hostingContext.Configuration.GetSection("ElasticSearch");
            string elasticSearchUri = configurationSection["ElasticsearchForwardServiceEndpoint"];
            string environment = configurationSection["Environment"];
            string applicationName = configurationSection["ElasticsearchApplicationName"];

            var callingAssembly = Assembly.GetCallingAssembly();

            loggerConfiguration
                .Enrich.WithProperty("Application", applicationName)
                .Enrich.WithProperty("StartAssembly", callingAssembly.FullName)
                .Enrich.WithProperty("AssemblyVersion", callingAssembly.GetName().Version.ToString())
                .Enrich.WithProperty("Environment", environment)
                .Enrich.FromLogContext()
                .Enrich.With<GenericLogEnricher>()
                .Filter.ByExcluding(c => c.Properties.Any(p => p.Value.ToString().Contains("swagger")))
                .Filter.ByExcluding(c => c.Properties.Any(p => p.Key == "RequestPath" && p.Value.ToString().Contains("health", StringComparison.CurrentCultureIgnoreCase)))
                .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(elasticSearchUri)));
        }
    }
}
