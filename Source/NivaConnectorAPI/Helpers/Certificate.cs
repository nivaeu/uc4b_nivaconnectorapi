﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Serilog;

namespace NivaConnectorApi.Helpers
{
    public class Certificate
    {
        public static X509Certificate2 GetClientCertificate(string certThumbprint)
        {
            try
            {
                bool validOnly = false;

                using (X509Store certStore = new X509Store(StoreName.My, StoreLocation.CurrentUser))
                {
                    certStore.Open(OpenFlags.ReadOnly);

                    X509Certificate2Collection certCollection = certStore.Certificates.Find(
                        X509FindType.FindByThumbprint,
                        certThumbprint,
                        validOnly);
                    // Get the first cert with the thumbprint
                    X509Certificate2 cert = certCollection.OfType<X509Certificate2>().FirstOrDefault();

                    if (cert is null)
                    {
                        Log.Logger
                            .ForContext("body", $"Certificate with thumbprint {certThumbprint} was not found")
                            .ForContext("message", $"Certificate with thumbprint {certThumbprint} was not found")
                            .Error("Certificate exception");
                        throw new Exception($"Certificate with thumbprint {certThumbprint} was not found");
                    }

                    certStore.Close();

                    // Use certificate
                    Log.Logger
                        .ForContext("body", cert.Thumbprint)
                        .ForContext("message", "cert.FriendlyName")
                        .Information(cert.Thumbprint);
                    return cert;
                }
            }
            catch (Exception ex)
            {
                Log.Logger
                    .ForContext("body", "Certificate exception")
                    .ForContext("message", ex.Message)
                    .Error("Certificate exception");
                return null;
            }
        }
    }
}
