﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Net;
using Microsoft.AspNetCore.Http;
using NivaConnectorApi.Helpers;

namespace NivaConnectorApi.Repository.Proprietary.PayingAgencyNL
{
	public class PayingAgencyApi : BaseWebApi
	{
		private string Username { get; } = Startup.Configuration["ServiceAccounts:PayingAgencyApi:Username"];

		private string Password { get; } = Startup.Configuration["ServiceAccounts:PayingAgencyApi:Password"];

		protected override string GetBaseUrl()
		{
			return Startup.Configuration["ExternalServiceBaseUrls:PayingAgencyApi"];
		}

		protected override void AddHeaders(HttpContext currentContext, HttpWebRequest httpWebRequest)
		{
			byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(Username + ":" + Password);

            AddCertificateIfNotLocaleHost(httpWebRequest);
			httpWebRequest.Headers.Add("Authorization", "Basic " + System.Convert.ToBase64String(plainTextBytes));
		}

        private void AddCertificateIfNotLocaleHost(HttpWebRequest httpWebRequest)
        {
            var environment = Startup.Configuration["Environment"].ToLower();
            if (environment != "localhost")
            {
                var certificateThumbprint = Startup.Configuration["ApplicationSettings:CertificateThumbprint"];
                var certificate = Certificate.GetClientCertificate(certificateThumbprint);
                if (certificate == null)
                    return;

                httpWebRequest.ServerCertificateValidationCallback = (sender, cert, chain, errors) => true;
                httpWebRequest.ClientCertificates.Add(certificate);
            }
        }
	}
}
