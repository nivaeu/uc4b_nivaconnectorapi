﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace NivaConnectorApi.Repository.Proprietary.SEGES
{
	public class PlantApi : BaseWebApi
	{
		protected override string GetBaseUrl()
		{
			return Startup.Configuration["ExternalServiceBaseUrls:SegesPlantApi"];
		}

		protected override void AddHeaders(HttpContext currentContext, HttpWebRequest httpWebRequest)
		{
			string authHeader = currentContext.Request.Headers["Authorization"].FirstOrDefault();

			if (string.IsNullOrEmpty(authHeader))
			{
				throw new ArgumentNullException("Authorization", "header field must be populated in order to pass-through value");
			}

			string token = authHeader.Split(" ").Last();
			httpWebRequest.Headers.Add("Authorization", "OAuth " + token);
		}
	}
}
