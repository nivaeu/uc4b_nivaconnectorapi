﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using NivaConnectorApi.Infrastructure;

namespace NivaConnectorApi.Repository
{
	public abstract class BaseWebApi
	{
		protected abstract string GetBaseUrl();
		protected abstract void AddHeaders(HttpContext currentContext, HttpWebRequest httpWebRequest);

		public async Task<RequestError> SendRequestAsync(HttpContext context, string verb, string url, string body = null)
		{
			try
			{
				HttpWebRequest httpWebRequest = GetWebRequest(context, verb, url, body);
				await httpWebRequest.GetResponseAsync();
			}
			catch (ArgumentNullException e)
			{
				return new RequestError
				{
					ErrorCode = (int)ApiErrorCode.MissingRequestInformation,
					ErrorMessage = "Missing request information: " + e.Message
				};
			}
			catch (WebException e)
			{
				Stream responseStream = e.Response.GetResponseStream();

				if (responseStream == null)
				{
					return new RequestError
					{
						ErrorCode = (int)ApiErrorCode.UnknownInternalError,
						ErrorMessage = e.Message
					};
				}

				string errorResponse = await new StreamReader(responseStream).ReadToEndAsync();

				return new RequestError
				{
					ErrorCode = (int)ApiErrorCode.UnknownExternalApiError,
					ErrorMessage = errorResponse
				};
			}

			return null;
		}

		public async Task<(RequestError,T)> SendRequestAsync<T>(HttpContext context, string verb, string url, string body = null)
		{
			try
			{
				HttpWebRequest httpWebRequest = GetWebRequest(context, verb, url, body);

				WebResponse httpWebResponse = await httpWebRequest.GetResponseAsync();
				Stream stream = httpWebResponse.GetResponseStream();
				if (stream == null)
				{
					return (
						new RequestError
						{
							ErrorCode = (int)ApiErrorCode.UnknownInternalError,
							ErrorMessage = "ResponseStream is null"
						},
						default(T)
					);
				}

				using (var streamReader = new StreamReader(stream))
				{
					string content = streamReader.ReadToEnd();
                    return (null, JsonConvert.DeserializeObject<T>(content));
				}
			}
			catch (ArgumentNullException e)
			{
				var requestError = new RequestError
				{
					ErrorCode = (int)ApiErrorCode.MissingRequestInformation,
					ErrorMessage = "Missing request information: " + e.Message
				};

				return (requestError, default);
			}
			catch (WebException e)
			{
				Stream responseStream = e.Response.GetResponseStream();
				if (responseStream == null)
				{
					var requestError = new RequestError
					{
						ErrorCode = (int)ApiErrorCode.UnknownInternalError,
						ErrorMessage = e.Message
					};

					return (requestError, default(T));
				}

				string errorResponse = await new StreamReader(responseStream).ReadToEndAsync();
				var externalRequestError = new RequestError
				{
					ErrorCode = (int)ApiErrorCode.UnknownExternalApiError,
					ErrorMessage = errorResponse
				};

				return (externalRequestError, default(T));
			}
		}

		private HttpWebRequest GetWebRequest(HttpContext context, string verb, string url, string body = null)
		{
			url = $"{GetBaseUrl()}{url}";
			var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
			httpWebRequest.ContentType = "application/json";
			httpWebRequest.Method = verb.ToUpper();
			httpWebRequest.Timeout = 1000 * 10 * 60;

            AddHeaders(context, httpWebRequest);

			if (verb.ToLower() == "post" || verb.ToLower() == "put" || verb.ToLower() == "patch")
			{
				using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
				{
					streamWriter.Write(body);
					streamWriter.Flush();
					streamWriter.Close();
				}
			}

			return httpWebRequest;
		}
    }
}
