﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Autofac.Extras.CommonServiceLocator;
using CommonServiceLocator;
using Microsoft.Extensions.DependencyInjection;

namespace NivaConnectorApi.Infrastructure
{
    public static class AutofacExtensions
    {
        public static IServiceProvider AddAutofac(this IServiceCollection services, Action<ContainerBuilder> autofacRegistration)
        {
            // Create the Autofac container builder.
            ContainerBuilder builder = new ContainerBuilder();
            builder.Populate(services);
            autofacRegistration?.Invoke(builder);

            // Build the container.
            IContainer ApplicationContainer = builder.Build();

            AutofacServiceLocator csl = new AutofacServiceLocator(ApplicationContainer);
            ServiceLocator.SetLocatorProvider(() => csl);

            return new AutofacServiceProvider(ApplicationContainer);
        }

    }

}
