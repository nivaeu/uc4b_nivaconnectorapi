﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.ComponentModel;
using System.Globalization;

namespace NivaConnectorApi.Infrastructure
{
	public class IntegerListParameterConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			IntegerListParameter result = new IntegerListParameter();
			if (value is string s)
			{
				foreach (string part in s.Split(','))
				{
					if (int.TryParse(part, out int id))
					{
						result.Add(id);
					}
				}

				return result;
			}
			return base.ConvertFrom(context, culture, value);
		}
	}
}
