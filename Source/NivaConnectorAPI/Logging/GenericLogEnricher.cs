﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using Serilog.Core;
using Serilog.Events;

namespace NivaConnectorApi.Logging
{
	public class GenericLogEnricher : ILogEventEnricher
	{
		private static string OsVersionPropertyName => "OSVersion";
		private static string MachineNamePropertyName => "MachineName";
		private static string BaseDirectoryPropertyName => "BaseDirectory";

		public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
		{
			if (logEvent == null)
			{
				throw new ArgumentNullException("logEvent");
			}

			AddProperty(logEvent, OsVersionPropertyName, GetOsVersion());
			AddProperty(logEvent, MachineNamePropertyName, GetMachineName());
			AddProperty(logEvent, BaseDirectoryPropertyName, GetBaseDirectory());
		}

		private void AddProperty(LogEvent logEvent, string propertyName, string value)
		{
			var property = new LogEventProperty(propertyName, new ScalarValue(value));
			logEvent.AddPropertyIfAbsent(property);
		}

		private string GetOsVersion()
		{
			return Environment.OSVersion.ToString();
		}

		private string GetMachineName()
		{
			return Environment.MachineName;
		}

		private string GetBaseDirectory()
		{
			try
			{
				return AppDomain.CurrentDomain.BaseDirectory;
			}
			catch
			{
				return string.Empty;
			}
		}
	}
}